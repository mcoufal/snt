/**
 * SNT project: Solving model of Social Golfer Problem using Constraint Programming
 * Author: Martin Coufal <xcoufa08@stud.fit.vutbr.cz>
 * Date: 13.05.2019
 * Description: Project in course SNT - Simulation Tools and Techniques at FIT BUT.
 * Main goal is to repeat an experiment from publication "Solving the Social Golfers
 * Problems by Constraint Programming in Sequential and Parallel" by Ke Liu, Sven
 * Loffler and Petra Hofstedt.
 */

package com.mcoufal.fit.snt.social_golfer;

import static org.chocosolver.solver.search.strategy.Search.domOverWDegSearch;
import static org.chocosolver.solver.search.strategy.Search.minDomLBSearch;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.math3.util.CombinatoricsUtils;
import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solution;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.constraints.extension.Tuples;
import org.chocosolver.solver.constraints.nary.alldifferent.AllDifferent;
import org.chocosolver.solver.variables.IntVar;

/**
 * Class implementing sequential constraint programming model of the Social
 * Golfer Problem(SGP) described in the paper mentioned above.
 */
public class SocialGolferCP {

	private static int SGP_G; // number of groups
	private static int SGP_S; // number of players in one group
	private static int SGP_W; // number of weeks
	private static String SEARCH_STRATEGY; // predefined search strategy
	private static int N; // groups x players

	/**
	 * Create model of the SGP instance and solve it using choco-solver.
	 *
	 * @param args - parameters of the SGP instance and predefined solver search
	 *             strategy.
	 */
	public static void main(String[] args) {

		parseArgs(args);
		Model model = new Model("Social Golfer Problem");

		// define decision variables
		IntVar[][] G = new IntVar[SGP_W][N];

		// Model initialization: constraint #1, constraint #2, matrix G bounds
		initializeModel(model, G);

		// define basic model constraints: constraints: #3, #5, #6, #7
		defineBasicModelConstraints(model, G);

		// define instance specific constraints: #8, #9, #10, #11, #12, #13, #14,
		// #15, #16
		defineInstanceSpecificContrains(model, G);

		// set search strategy and solve
		Solver solver = model.getSolver();
		setSearchStrategy(solver, model.retrieveIntVars(false));
		solver.showStatistics();
		solver.showSolutions();
		Solution solution = solver.findSolution();

		// print results
		if (solution != null)
			System.out.println(solution.toString());
		printDecisionVariables(G);
		checkSolutionIsValid(G);
	}

	/**
	 * SGP model initialization. This corresponds to equations #1, #2 and setting
	 * correct bounds to decision variables of the matrix G.
	 *
	 * @param model - model of the SGP
	 * @param G     - decision variables matrix
	 */
	private static void initializeModel(Model model, IntVar[][] G) {

		System.out.println("adding basic constraints: #1, #2");

		// constraint #1 - initialization
		// 0 <= j < n, G[0][j] = j/s + 1
		for (int j = 0; j < N; j++) {
			G[0][j] = model.intVar(String.format("G[0][%d]", j), j / SGP_S + 1);
		}

		// constraint #2 - initialization
		// 0 < i < w, 0 <= j < s, G[i][j] = j + 1
		for (int i = 1; i < SGP_W; i++) {
			for (int j = 0; j < SGP_S; j++) {
				G[i][j] = model.intVar(String.format("G[%d][%d]", i, j), j + 1);
			}
		}

		// initialize possible values of the rest
		for (int i = 0; i < SGP_W; i++) {
			for (int j = 0; j < N; j++) {
				if (G[i][j] == null) {
					G[i][j] = model.intVar(String.format("G[%d][%d]", i, j), 1, SGP_G);
				}
			}
		}
	}

	/**
	 * Create basic model described by constraints #3, #5, #6, #7.
	 *
	 * @param model - model of the SGP
	 * @param G     - decision variables matrix
	 */
	private static void defineBasicModelConstraints(Model model, IntVar[][] G) {

		System.out.println("adding basic constraints: #3, #5, #6, #7");

		// constraint #3
		// 0 < i < w, V = {1..g}, O = [s,..,s], GCC(G[i][*], V, O)
		int[] V = new int[SGP_G];
		IntVar[] O = new IntVar[SGP_G];
		for (int i = 0; i < SGP_G; i++) {
			V[i] = i + 1;
			O[i] = model.intVar(String.format("O[%d]", i), SGP_S);
		}
		for (int i = 1; i < SGP_W; i++) {
			model.globalCardinality(G[i], V, O, true).post();
		}

		// constraint #5
		// 0 <= i < w, 0 <= j1 < j2 < n, 0 <= j3 < nCr(n, 2):
		// For each j1, j2: G[i][j1] - G[i][j2] = C[i][j3]
		int m = Math.toIntExact(CombinatoricsUtils.binomialCoefficient(N, 2));
		IntVar C[][] = new IntVar[SGP_W][m];
		for (int i = 0; i < SGP_W; i++) {
			for (int j = 0; j < m; j++) {
				C[i][j] = model.intVar(String.format("C[%d][%d]", i, j), -SGP_G, SGP_G);
			}
		}

		for (int i = 0; i < SGP_W; i++) {
			int j3 = 0;
			for (int j1 = 0; j1 < N - 1; j1++) {
				for (int j2 = j1 + 1; j2 < N; j2++) {
					model.arithm(G[i][j1], "-", G[i][j2], "=", C[i][j3]).post();
					j3++;
				}
			}
		}

		// constraint #6
		// 0 <= j < m, occ = {0,1}, count(C[*][j], occ, 0)
		IntVar[][] CT = new IntVar[m][SGP_W];
		CT = getTransposedMatrix(C, SGP_W, m);
		IntVar occ = model.intVar(0, 1);
		for (int j = 0; j < m; j++) {
			model.count(0, CT[j], occ).post();
		}

		// constraint #7
		// 0 < i < w, for each j != j' & j/s = j'/s: G[i][j] != G[i][j']
		for (int i = 1; i < SGP_W; i++) {
			for (int j = 0; j < N; j++) {
				for (int k = 0; k < N; k++)
					if (j != k && j / SGP_S == k / SGP_S) {
						model.allDifferent(new IntVar[] { G[i][j], G[i][k] }, AllDifferent.DEFAULT).post();
					}
			}
		}
	}

	/**
	 * Create more complex model for specific SGP instances. This corresponds to
	 * constraints #8, #9, #10, #11, #12, #13, #14, #15, #16.
	 * 
	 * @param model - model of the SGP
	 * @param G     - decision variables matrix
	 */
	private static void defineInstanceSpecificContrains(Model model, IntVar[][] G) {
		// constraints for instances of (s-s-s+1)
		if (isInstanceOf_S_S_S1(SGP_G, SGP_S, SGP_W)) {

			System.out.println("adding constraints for: s-s-(s+1): #8, #9");

			// constraint #8
			// 0 < i < i', s <= j, for all: i, i',j: G[i][j] != G[i'][j]
			for (int j = SGP_S; j < N; j++) {
				IntVar[] allDiff = new IntVar[SGP_W - 1];
				for (int i = 1; i < SGP_W; i++) {
					allDiff[i - 1] = G[i][j];
				}
				model.allDifferent(allDiff, AllDifferent.DEFAULT).post();
			}

			// constraint #9
			// s <= j < n, G[1][j] = j%s + 1
			for (int j = SGP_S; j < N; j++) {
				model.arithm(G[1][j], "=", j % SGP_S + 1).post();
			}

			// to fit the model in the paper, some instances don't need additional
			// constraints
			if (isInstanceOf(5, 5, 6) || isInstanceOf(7, 7, 8) || SGP_S <= 3)
				return;

			// constraints #10, #11: if building GS_2 manually with another model,
			// there is no need to execute these constrains twice!
			if (!isInstanceOf(8, 8, 9)) {

				if (!isInstanceOf_P_P_P1(SGP_G, SGP_S, SGP_W)) {

					System.out.println("adding constraints for: s-s-(s+1): #10, #11");

					// constraint #10
					// formula is wrong: it seems to be forgetting to apply the rules only to the
					// sub-matrix GS_2: hence +1/-1 to forget first line
					// 0 < i < w, s <= j < 2s, for all: i, j: G[i][j] != G[j - s][i + s]
					for (int i = 2; i < SGP_W; i++) {
						for (int j = SGP_S; j < 2 * SGP_S; j++) {
							model.arithm(G[i][j], "=", G[j - SGP_S + 1][i + SGP_S - 1]).post();
						}
					}

					// constraint #11
					// formula is wrong: it seems to be forgetting to apply the rules only to the
					// main diagonal of the sub-matrix GS_2: hence iterator j is changed
					// 0 < i < w, s <= j < 2s, for all: i, j: G[i][j] != G[i + 1][j + 1]
					for (int i1 = 1; i1 < SGP_W - 1; i1++) {
						for (int i2 = i1; i2 < SGP_W; i2++) {
							if (i1 == i2)
								continue;
							model.allDifferent(new IntVar[] { G[i1][i1 + SGP_S - 1], G[i2][i2 + SGP_S - 1] },
									AllDifferent.DEFAULT).post();
						}
					}
				}

				System.out.println("adding constraints for: s-s-(s+1): #12");

				// constraint #12
				// 1 < i < w, s <= j < j' < n, j%s = j'%s, for all: i, j, j': G[i][j] !=
				// G[i][j']
				for (int i = 2; i < SGP_W; i++) {
					IntVar[] allDiff = new IntVar[SGP_G - 1];
					int index = 0;
					int j = SGP_S;
					allDiff[index] = G[i][j];
					for (int k = SGP_S + 1; k < N; k++) {
						if (j % SGP_S == k % SGP_S) {
							index++;
							allDiff[index] = G[i][k];
						}
					}
					model.allDifferent(allDiff, AllDifferent.DEFAULT).post();
				}
			}
		}

		// constraints for instances of (p-p-p+1)
		if (isInstanceOf_P_P_P1(SGP_G, SGP_S, SGP_W)) {

			System.out.println("adding constraints for: p-p-(p+1): #13, #14");

			// new model for sub-matrix GS_2
			Model modelGS2 = new Model("SGP GS_2 submatrix");
			IntVar[][] GS2 = new IntVar[SGP_S][SGP_S];

			// solve sub-matrix GS_2
			solveGS2(modelGS2, GS2);

			// solved GS_2, we can copy the values to G
			for (int i = 1; i < SGP_W; i++) {
				for (int j = SGP_S; j < 2 * SGP_S; j++) {
					model.arithm(G[i][j], "=", GS2[i - 1][j - SGP_S].getValue());
				}
			}

			// constraint #13
			// PT = {(G[i][j], G[i][j + 1], ..., G[i][j + s]) | s <= j < 2s, 2 <= i < w}
			Tuples pt = new Tuples();
			for (int i = 1; i < SGP_S; i++) {
				int[] tuplesIntArray = new int[SGP_S];
				for (int j = 0; j < SGP_S; j++) {
					tuplesIntArray[j] = GS2[i][j].getValue();
				}
				pt.add(tuplesIntArray);
			}

			// constraint #14
			// 2 <= i < w, 2s <= j < n, j % s = 0, (G[i][j], G[i][j + 1], ..., G[i][j + s -
			// 1]) is in PT
			for (int i = 2; i < SGP_W; i++) {
				for (int j = 2 * SGP_S; j < N; j++) {
					if (j % SGP_S == 0) {
						IntVar[] hlp = new IntVar[SGP_S];
						for (int k = 0; k < SGP_S; k++) {
							hlp[k] = G[i][j + k];
						}
						// add table constraint for each GP_i row
						model.table(hlp, pt).post();
					}
				}
			}
		}

		// constraints for instances of (e-e-e+1)
		if (isInstanceOf_E_E_E1(SGP_G, SGP_S, SGP_W) && !isInstanceOf(6, 6, 7)) {

			System.out.println("adding constraints for: e-e-(e+1): #15, #16");

			// constraint #15
			// 0 < i < w, G[i][i + s - 1] = 1
			for (int i = 2; i < SGP_W; i++) {
				model.arithm(G[i][i + SGP_S - 1], "=", 1).post();
			}

			// constraint #16
			// 0 < i < i' < w, 2s <= j < j' < n, for all: j / s = j' / s & j % s + 1 = i &
			// j' % s + 1 = i', G[i][j] != G[i'][j']
			for (int k = 2; k < SGP_G; k++) {
				int j1 = SGP_S * k;
				for (int i1 = 1; i1 < SGP_W - 1; i1++) {
					int j2 = j1;
					for (int i2 = i1; i2 < SGP_W; i2++) {
						if (i1 == i2) {
							j2++;
							continue;
						}
						model.allDifferent(new IntVar[] { G[i1][j1], G[i2][j2] }, AllDifferent.DEFAULT).post();
						j2++;
					}
					j1++;
				}
			}
		}
	}

	/**
	 * Check whether created model is instance of SGP problem of the form s-s-(s+1).
	 * 
	 * @param g - number of groups
	 * @param s - number of players in each group
	 * @param w - number of weeks in the tournament
	 * @return true if created model fits the form s-s-(s+1), false otherwise.
	 */
	private static boolean isInstanceOf_S_S_S1(int g, int s, int w) {
		if (g == s && g == w - 1) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Check whether created model is instance of SGP problem of the form
	 * prime-prime-(prime+1).
	 * 
	 * @param g - number of groups
	 * @param s - number of players in each group
	 * @param w - number of weeks in the tournament
	 * @return true if created model fits the form prime-prime-(prime+1), false
	 *         otherwise.
	 */
	private static boolean isInstanceOf_P_P_P1(int g, int s, int w) {
		if (isInstanceOf_S_S_S1(g, s, w)) {
			boolean isPrime = true;
			for (int div = 2; div < s; div++) {
				if (s % div == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Check whether created model is instance of SGP problem of the form
	 * even-even-(even+1).
	 * 
	 * @param g - number of groups
	 * @param s - number of players in each group
	 * @param w - number of weeks in the tournament
	 * @return true if created model fits the form even-even-(even+1), false
	 *         otherwise.
	 */
	private static boolean isInstanceOf_E_E_E1(int g, int s, int w) {
		if (isInstanceOf_S_S_S1(g, s, w)) {
			if (SGP_S % 2 == 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * Test whether SGP model instance given by g-s-w fits instance
	 * exp_g-exp_s-exp-w.
	 * 
	 * @param exp_g - expected number of groups
	 * @param exp_s - expected number of players in each group
	 * @param exp_w - expected number of weeks in the tournament
	 * @return
	 */
	private static boolean isInstanceOf(int exp_g, int exp_s, int exp_w) {
		if (SGP_G == exp_g && SGP_S == exp_s && SGP_W == exp_w) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Get transposed matrix of the original matrix given by parameter 'matrix' of
	 * the size 'sizeX' x 'sizeY'.
	 *
	 * @param matrix - original matrix
	 * @param sizeX  - number of rows in the original matrix
	 * @param sizeY  - number of columns in the original matrix
	 * @return transposed matrix
	 */
	private static IntVar[][] getTransposedMatrix(IntVar[][] matrix, int sizeX, int sizeY) {
		IntVar tmp[][] = new IntVar[sizeY][sizeX];
		for (int i = 0; i < sizeX; i++) {
			for (int j = 0; j < sizeY; j++) {
				tmp[j][i] = matrix[i][j];
			}
		}
		return tmp;
	}

	/**
	 * Solve sub-matrix GS_2 of matrix G.
	 * 
	 * @param modelGS2
	 * @param GS2
	 */
	private static void solveGS2(Model modelGS2, IntVar[][] GS2) {

		// initialize first row
		for (int j = 0; j < SGP_S; j++) {
			GS2[0][j] = modelGS2.intVar(String.format("GS_2[0][%d]", j), j + 1);
		}

		// initialize second row
		GS2[1][0] = modelGS2.intVar(String.format("GS_2[1][%d]", 0), 2);
		GS2[1][1] = modelGS2.intVar(String.format("GS_2[1][%d]", 1), SGP_S);
		GS2[1][2] = modelGS2.intVar(String.format("GS_2[1][%d]", 2), 1);
		for (int j = 3; j < SGP_S; j++) {
			GS2[1][j] = modelGS2.intVar(String.format("GS_2[1][%d]", j), j);
		}

		// last row fix
		GS2[SGP_S - 1][2] = modelGS2.intVar(String.format("GS_2[%d][%d]", SGP_S - 1, 2), 2);
		GS2[SGP_S - 1][3] = modelGS2.intVar(String.format("GS_2[%d][%d]", SGP_S - 1, 3), 1);
		for (int j = 4; j < SGP_S; j++) {
			GS2[SGP_S - 1][j] = modelGS2.intVar(String.format("GS_2[%d][%d]", SGP_S - 1, j), j - 1);
		}
		// propagation in rows before last
		for (int i = SGP_S - 2; i > 1; i--) {
			for (int j = SGP_S - i + 1; j < SGP_S; j++) {
				GS2[i][j] = modelGS2.intVar(String.format("GS_2[%d][%d]", i, j), GS2[i + 1][j - 1].getValue());
			}
		}

		// initialize rest of variables
		for (int i = 0; i < SGP_S; i++) {
			for (int j = 0; j < SGP_S; j++) {
				if (GS2[i][j] == null) {
					GS2[i][j] = modelGS2.intVar(String.format("GS_2[%d][%d]", i, j), 1, SGP_S);
				}
			}
		}

		// constraint #10, but for matrix GS_2
		for (int i = 0; i < SGP_S; i++) {
			for (int j = 0; j < SGP_S; j++) {
				modelGS2.arithm(GS2[i][j], "=", GS2[j][i]).post();
			}
		}

		// constraint #11, but for matrix GS_2
		for (int i1 = 0; i1 < SGP_S; i1++) {
			for (int i2 = i1; i2 < SGP_S; i2++) {
				if (i1 == i2)
					continue;
				modelGS2.allDifferent(new IntVar[] { GS2[i1][i1], GS2[i2][i2] }, AllDifferent.DEFAULT).post();
			}
		}

		// allDifferent constraint on each row
		for (int i = 0; i < SGP_S; i++) {
			IntVar[] allDiff = new IntVar[SGP_S];
			for (int j = 0; j < SGP_S; j++) {
				allDiff[j] = GS2[i][j];
			}
			modelGS2.allDifferent(allDiff, AllDifferent.DEFAULT).post();
		}

		// allDifferent constraint on each column
		for (int j = 0; j < SGP_S; j++) {
			IntVar[] allDiff = new IntVar[SGP_S];
			for (int i = 0; i < SGP_S; i++) {
				allDiff[i] = GS2[i][j];
			}
			modelGS2.allDifferent(allDiff, AllDifferent.DEFAULT).post();
		}

		// set strategy and solve
		Solver gs2solver = modelGS2.getSolver();
		setSearchStrategy(gs2solver, modelGS2.retrieveIntVars(false));
		gs2solver.showStatistics();
		gs2solver.showSolutions();
		Solution solution = gs2solver.findSolution();

		// print results
		if (solution != null)
			System.out.println(solution.toString());
		printMatrix(GS2, SGP_S, SGP_S);
	}

	/**
	 * Set search strategy according to SEARCH_STRATEGY value.
	 * 
	 * @param solver  - solver we want to use strategy for
	 * @param allVars - array of all variables used in the model
	 */
	private static void setSearchStrategy(Solver solver, IntVar[] allVars) {
		if (SEARCH_STRATEGY.equals("min")) {
			solver.setSearch(minDomLBSearch(allVars));
		} else if (SEARCH_STRATEGY.equals("dom")) {
			solver.setSearch(domOverWDegSearch(allVars));
		}
	}

	/**
	 * Check if the solution is valid. I.e. no two golfers play in the same group
	 * more than once.
	 *
	 * @param G - two-dimensional array of decision variables (weeks x N)
	 * @return true, if the solution is valid, false otherwise.
	 */
	private static boolean checkSolutionIsValid(IntVar[][] G) {
		boolean[][] hlpTable = new boolean[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				hlpTable[i][j] = false;
			}
		}
		// for each player
		for (int p = 0; p < N; p++) {
			// for each week
			for (int w = 0; w < SGP_W; w++) {
				int pGroup = G[w][p].getValue();
				// for all other players
				for (int j = 0; j < N; j++) {
					// same player, continue
					if (p == j)
						continue;
					// is in same group
					if (G[w][j].getValue() == pGroup) {
						// was in the same group before -> fail
						if (hlpTable[p][j]) {
							System.out.println(String.format(
									"FAIL: Player_%d plays with Player_%d in same group for the second time! Week: %d, Group: %d",
									p + 1, j + 1, w + 1, pGroup));
							return false;
						} else {
							hlpTable[p][j] = true; // mark a pair of players that have played together
						}
					}
				}
			}
		}
		System.out.println("OK!");
		return true;
	}

	/**
	 * Print decision variable matrix G of the SGP instance to the standard output.
	 *
	 * @param G - decision variables matrix [weeks][players x groups]
	 */
	private static void printDecisionVariables(IntVar[][] G) {

		System.out.println("Decision variables G[weeks][players x groups]");
		// for every week
		for (int i = 0; i < SGP_W; i++) {
			// for every player
			for (int j = 0; j < N; j++) {
				if (j % SGP_S == 0) {
					if (j != 0)
						System.out.print("  |");
					else
						System.out.print("|");
				}
				if (G[i][j] != null) {
					int val = G[i][j].getValue();
					if (val < 10)
						System.out.print(String.format(" %d|", val));
					else
						System.out.print(String.format("%d|", val));
				} else {
					System.out.print(" ?|");
				}
			}
			System.out.print("\n");
		}
	}

	/**
	 * Print matrix 'm' to the standard output.
	 *
	 * @param m     - two-dimensional IntVar matrix to print
	 * @param sizeX - number of rows
	 * @param sizeY - number of columns
	 */
	private static void printMatrix(IntVar[][] m, int sizeX, int sizeY) {

		System.out.println(String.format("Matrix[%d][%d]", sizeX, sizeY));
		for (int i = 0; i < sizeX; i++) {
			System.out.print("|");
			for (int j = 0; j < sizeY; j++) {
				if (m[i][j] == null) {
					System.out.print(" ?|");
				} else {
					int val = m[i][j].getValue();
					if (val < 10)
						System.out.print(String.format(" %d|", val));
					else
						System.out.print(String.format("%d|", val));
				}
			}
			System.out.print("\n");
		}
	}

	/**
	 * Parse command line arguments.
	 * 
	 * This application takes 4 required arguments: 1) g: number of groups 2) p:
	 * number of players in each group 3) w: number of weeks in the tournament 4) s:
	 * predefined search strategy
	 * 
	 * @param args - four required arguments g, p, w, s.
	 */
	private static void parseArgs(String[] args) {

		// define all possible options
		Options options = new Options();

		Option optGroups = new Option("g", "groups", true, "number of groups");
		optGroups.setRequired(true);
		options.addOption(optGroups);

		Option optPlayers = new Option("p", "players", true, "number of players in each group");
		optPlayers.setRequired(true);
		options.addOption(optPlayers);

		Option optWeeks = new Option("w", "weeks", true, "number of weeks in the tournament");
		optWeeks.setRequired(true);
		options.addOption(optWeeks);

		Option optStrategy = new Option("s", "strategy", true,
				"predefined search strategy ('min' or 'dom' are available - minDomLBSearch/domOverWDegSearch)");
		optStrategy.setRequired(false);
		options.addOption(optStrategy);

		// parse them
		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd = null;

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			System.out.println(e.getStackTrace());
			formatter.printHelp("SocialGolferCP", options);
			System.exit(1);
		}

		// initialize global variables
		SGP_G = Integer.parseInt(cmd.getOptionValue("groups"));
		SGP_S = Integer.parseInt(cmd.getOptionValue("players"));
		SGP_W = Integer.parseInt(cmd.getOptionValue("weeks"));
		SEARCH_STRATEGY = cmd.getOptionValue("strategy") != null ? cmd.getOptionValue("strategy") : "default";
		N = SGP_G * SGP_S;

		if (!SEARCH_STRATEGY.equals("dom") && !SEARCH_STRATEGY.equals("min") && SEARCH_STRATEGY != "default") {
			System.out.println("Unknown search strategy option! See help for details.");
			formatter.printHelp("SocialGolferCP", options);
			System.exit(1);
		}

		System.out.println(String.format("Social Golfer Problem: %d-%d-%d (%s)", SGP_G, SGP_S, SGP_W, SEARCH_STRATEGY));
	}
}
