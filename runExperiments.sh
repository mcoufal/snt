#!/usr/bin/env bash

RUNNABLE="social-golfer.jar"
DIR_SIM="simulation_runs"
EXP_NAME="sgp-${1}-${2}-${3}"
NUM_OF_RUNS=10 # number of runs of a single simulation
MEDIAN=$((NUM_OF_RUNS / 2))
EXPS_CLEAN=true

# check arguments, setup strategy
if [ $# -eq 3 ]; then
  STRATEGY_DEFINED=false
elif [ $# -eq 4 ]; then
  EXP_NAME="sgp-${1}-${2}-${3}-${4}"
  STRATEGY_DEFINED=true
else
  echo "Error: wront arguments!"
  exit 1
fi

# setup environment
if [ ! -d "$DIR_SIM" ]; then
  mkdir $DIR_SIM
fi

if [ ! -d "$DIR_SIM/$EXP_NAME" ]; then
  mkdir $DIR_SIM/$EXP_NAME
fi

# run experiments
for i in `seq 1 $NUM_OF_RUNS`; do
  timestamp=`date +"%T"`
  echo "[$timestamp] Running experiment $i/$NUM_OF_RUNS..."

  if $STRATEGY_DEFINED; then
    java -jar $RUNNABLE -g ${1} -p ${2} -w ${3} -s ${4} > "$DIR_SIM/$EXP_NAME/result_$i.log"
  else
    java -jar $RUNNABLE -g ${1} -p ${2} -w ${3} > "$DIR_SIM/$EXP_NAME/result_$i.log"
  fi

  if [ $? -eq 0 ]; then
    cat $DIR_SIM/$EXP_NAME/result_$i.log | grep -q "FAIL:"
    if [ $? -eq 0 ]; then
      echo "[FAIL]: solution not found! (unsolvable)"
    else
      echo "[OK]"
    fi
  else
    EXPS_CLEAN=false
    echo "[ERROR]: see 'result_$i.log' for details"
  fi
done
echo "...simulations run results are stored at: $DIR_SIM/$EXP_NAME/"


# get results
if $EXPS_CLEAN; then
  rm -rf $DIR_SIM/$EXP_NAME/final_results.log # cleanup
  echo "-----------------------"
  echo " Evaluating results..."

  # get used constraints
  constraints=`cat $DIR_SIM/$EXP_NAME/result_1.log | grep "constraints" | grep -Eo '#[0-9]{1,2}'`

  # get median of resolution times
  resolution_time=`cat $DIR_SIM/$EXP_NAME/result_*.log | grep "Resolution time :" | grep -o -E '[0-9]+(\.[0-9]+)?' | sort -n | sed -n "${MEDIAN}p"`
  # get median of created nodes (should be the same, but anyway...)
  nodes=`cat $DIR_SIM/$EXP_NAME/result_*.log | grep "Nodes:" | cut -d " " -f 2 | sort -n | sed -n "${MEDIAN}p"`
  # get median of backtracks (should be the same, but anyway...)
  backtracks=`cat $DIR_SIM/$EXP_NAME/result_*.log | grep "Backtracks:" | cut -d " " -f 2 | sort -n | sed -n "${MEDIAN}p"`
  # get median of fails (should be the same, but anyway...)
  fails=`cat $DIR_SIM/$EXP_NAME/result_*.log | grep "Fails:" | cut -d " " -f 2 | sort -n | sed -n "${MEDIAN}p"`

  # store results
  echo "----- Used constraints -----" >> $DIR_SIM/$EXP_NAME/final_results.log
  echo ${constraints} | sed 's/ /, /g' >> $DIR_SIM/$EXP_NAME/final_results.log
  echo "--------- Results ----------" >> $DIR_SIM/$EXP_NAME/final_results.log
  echo "Instance ${1}-${2}-${3} results:" >> $DIR_SIM/$EXP_NAME/final_results.log
  echo "* Time: ${resolution_time}s" >> $DIR_SIM/$EXP_NAME/final_results.log
  echo "* Nodes: ${nodes}" >> $DIR_SIM/$EXP_NAME/final_results.log
  echo "* Backtracks: ${backtracks}" >> $DIR_SIM/$EXP_NAME/final_results.log
  echo "* Fails: ${fails}" >> $DIR_SIM/$EXP_NAME/final_results.log
  if $STRATEGY_DEFINED; then
    echo "* Strategy: ${4}" >> $DIR_SIM/$EXP_NAME/final_results.log
  else
    echo "* Strategy: default" >> $DIR_SIM/$EXP_NAME/final_results.log
  fi
  cat $DIR_SIM/$EXP_NAME/final_results.log
  echo "[DONE]: results are stored at: $DIR_SIM/$EXP_NAME/final_results.log"

else
  echo "One or more experiments have failed, aborting results evaluation!"
fi
