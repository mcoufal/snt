#!/usr/bin/env bash

DIR_SIM="simulation_runs"

if [ ! -d "$DIR_SIM" ]; then
  echo "Nothing to do..."
else
  rm -rf $DIR_SIM
fi
